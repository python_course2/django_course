"""django_course URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include

from shops.urls import router

from rest_framework_swagger.views import get_swagger_view

from shops.views import *

schema_view = get_swagger_view(title='Pastebin API')
urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^chaining/', include('smart_selects.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('shop/', include('shops.urls')),
    path('swagger/', schema_view),
    path('token/', LoginTokenView.as_view()),
    path('send/sms/', SendSmsToPhoneView.as_view({'post': 'create'})),
    path('check/sms/', LoginWithPhoneView.as_view({'post': 'create'}))
]
