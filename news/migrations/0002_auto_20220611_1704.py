# Generated by Django 3.2 on 2022-06-11 11:04

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='news',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='news.news'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2022, 6, 11, 17, 4, 11, 485937)),
        ),
        migrations.AlterField(
            model_name='news',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2022, 6, 11, 17, 4, 11, 485367)),
        ),
    ]
