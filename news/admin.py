#be c


from django.contrib import admin

from news.models import News, Comment


class CommentInnline(admin.TabularInline):
    model = Comment
    extra = 0
    can_delete = False


class NewsAdmin(admin.ModelAdmin):
    inlines = [CommentInnline]
    # exclude = ['image', 'name']
    ordering = ['-name', 'id']
    # prepopulated_fields = {"slug": ("name",)}
    # fields = (('name', 'created_date'),('file', 'is_active', 'image'))
    # fields = ['name', 'created_date']
    list_display = ['name', 'created_date']
    list_display_links = ['created_date']
    list_editable = ['name']
    search_fields = ['name']
    list_per_page = 3
    fieldsets = (
        ('Content', {
            'fields': ('name', 'text', 'created_date')
        }),
        ('Файлдар', {
            'fields': ('image', 'file'),
        }),
    )


class CommentAdmin(admin.ModelAdmin):
    model = Comment
    list_display = ['title', 'get_news_name']
    list_filter = ['news']
    autocomplete_fields = ['news']
    def get_news_name(self, obj):
        return obj.news.name



admin.site.register(News, NewsAdmin)
admin.site.register(Comment,CommentAdmin)
