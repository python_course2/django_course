import datetime
import random
from datetime import timezone
from django.contrib.auth import login
from django.contrib.auth.hashers import check_password
from django.shortcuts import render
from rest_framework.authtoken.admin import User

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
# Create your views here.
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from shops.models import *
from shops.serializers import *
from user.permissions import StudentPermission, ManagerPermission, TestPermission


class SubCategoryViewSet(ModelViewSet):
    serializer_class = SubCategorySerializer
    queryset = SubCategory.objects.all()
    permission_classes = [AllowAny]
    lookup_field = 'id'

    # def get_serializer_class(self):
    #     if self.action == 'create':
    #         return SubCategoryCreateSerializer()
    #     else:
    #         return SubCategorySerializer()


class CategoryViewSet(ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    permission_classes = [TestPermission|StudentPermission]
    lookup_field = 'id'

    # @action(detail=False, methods=['get'])
    # def category_all(self, request):
    #     queryset = Category.objects.all().filter(id__gt=1)
    #     return Response(CategorySerializer(queryset, many=True).data)

    def get_serializer_class(self):
        if self.action == 'list':
            self.serializer_class = CategorySerializer
        elif self.action == 'retrieve':
            self.serializer_class = CategoryDetailSerializer
        return super(CategoryViewSet, self).get_serializer_class()

    def get_permissions(self):
        if self.action in ['list', 'retrieve']:
            self.permission_classes = [IsAuthenticated & StudentPermission | ManagerPermission]
        else:
            self.permission_classes = [IsAuthenticated, ManagerPermission]
        return super(CategoryViewSet, self).get_permissions()

    def get_queryset(self):
        queryset = Category.objects.filter(id__lt=len(self.request.user.username))
        return queryset

class LoginTokenView(CreateAPIView):
    queryset = User.objects.all()
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        login = request.data['login']
        password = request.data['password']
        user = User.objects.filter(username=login)
        if len(user) > 0:
            if check_password(password, user[0].password):
                token = Token.objects.filter(user=user[0])
                if token:
                    token.delete()
                token = Token(user=user[0])
                token.save()
                return Response({
                    'token': token.key,
                    'user_id': user[0].id
                })
            else:
                return Response({
                    'error': "password not true"
                })

        return Response({
            'error': "login is not True"
        })


class SendSmsToPhoneView(ModelViewSet):
    queryset = User.objects.all()
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        phone = request.data['phone']
        user = User.objects.filter(username=phone)
        code = str(random.randint(1000, 9999))
        now = datetime.datetime.now(timezone.utc)
        if user:
            user = user[0]
            if (user.last_login and user.last_login + datetime.timedelta(minutes=1) < now) or user.last_login is None:
                user.last_login = now
                user.first_name = code
                user.save()
            else:
                return Response({'error': "anan jonotosun!"}, status=400)
        else:
            new_user = User(username=phone, first_name=code, is_active=True)
            new_user.save()
        return Response({'code': code})


class LoginWithPhoneView(ModelViewSet):
    queryset = User.objects.all()
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        phone = request.data['phone']
        code = request.data['code']
        user = User.objects.filter(username=phone, first_name=code)
        if len(user) > 0 and len(user[0].first_name) > 3:
            user[0].first_name = ''
            user[0].save()
            token = Token.objects.filter(user=user[0])
            if token:
                token.delete()
            token = Token(user=user[0])
            token.save()
            return Response({
                'token': token.key,
                'user_id': user[0].id
            })
        else:
            return Response({
                'error': "Code not correct"
            })
