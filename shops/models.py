from django.db import models
from smart_selects.db_fields import ChainedForeignKey


class Category(models.Model):
    title = models.CharField(max_length=255)
    def __str__(self):
        return self.title


class SubCategory(models.Model):
    category = models.ForeignKey(Category, models.CASCADE)
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title


class Currency(models.Model):
    title = models.CharField(max_length=255)


class Product(models.Model):
    title = models.CharField(max_length=255)
    category = models.ForeignKey(Category, models.SET_NULL, null=True, blank=True)
    sub_category = ChainedForeignKey(
        SubCategory,
        chained_field="category",
        chained_model_field="category",
        show_all=False,
        auto_choose=True,
        sort=True
    )
    article_number = models.IntegerField()
    price = models.IntegerField()
    currency = models.ForeignKey(Currency, models.SET_NULL, null=True, blank=True)
    photo = models.ImageField(upload_to='product/')


class Order(models.Model):
    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"
        ordering = ["-id"]
    ORDER_TYPE_TWO = (
        ('cash', 'Нал'),
        ('card', 'Карта'),
        ('odengi', 'О денги'),
    )
    name = models.CharField(max_length=100)
    is_delivery = models.BooleanField(default=True)
    order_type = models.CharField(max_length=40, choices=ORDER_TYPE_TWO, null=True)
    total_price = models.IntegerField()
    address = models.TextField()
    total_item = models.IntegerField()


class ItemOrder(models.Model):
    product = models.ForeignKey(Product, models.SET_NULL, null=True, blank=True)
    product_name = models.CharField(max_length=255)
    order = models.ForeignKey(Order, models.SET_NULL, null=True, blank=True)
    quantity = models.IntegerField()

