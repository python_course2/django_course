from rest_framework import serializers

from shops.models import Category, SubCategory


class CategoryCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ['title']

    def validate(self, attrs):
        if len(attrs['title']) < 5:
            raise serializers.ValidationError("category title must be long 5!")
        return attrs



class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ['id', 'title']


class SubCategorySerializer(serializers.ModelSerializer):
    # category = CategorySerializer()

    class Meta:
        model = SubCategory
        fields = ['id', 'title', 'category']
        depth = 1


class SubCategoryCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubCategory
        fields = ['id', 'title', 'category']


class CategoryDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ['title']

    def to_representation(self, obj):
        representation = super().to_representation(obj)
        representation['len'] = len(obj.title)
        return representation




