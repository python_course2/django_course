# from django.db import router
from django.urls import path, include
from rest_framework import routers

from shops.views import *

router = routers.DefaultRouter()
router.register(r'category', CategoryViewSet)
router.register(r'sub_category', SubCategoryViewSet)


urlpatterns = [
    path('', include(router.urls)),
    # path('category_all/', CategoryViewSet.as_view({'get': 'category_all'})),
    # path('category/<int:id>/', CategoryViewSet.as_view({'get': 'list', 'post': 'create'}))
]
