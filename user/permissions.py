from rest_framework.permissions import BasePermission

from user.models import STUDENT, MANAGER


class StudentPermission(BasePermission):
    def has_permission(self, request, view):
        if request.user.role == STUDENT:
            return True
        else:
            return False


class ManagerPermission(BasePermission):
    def has_permission(self, request, view):
        if request.user.role == MANAGER:
            return True
        else:
            return False


class TestPermission(BasePermission):
    def has_permission(self, request, view):
        if len(request.user.username) > 3:
            return True
        else:
            return False
