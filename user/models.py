from django.contrib.auth.models import AbstractUser
from django.db import models

STUDENT = 'student'
MANAGER = 'manager'
USER_ROLE = ((STUDENT, 'Student'), (MANAGER, 'Bashkaruchu'))


class User(AbstractUser):
    class Meta:
        db_table = 'my_user'
    role = models.CharField(max_length=20, choices=USER_ROLE)
